/**
 * @file
 * @brief Header for the Status enum
 */

#ifndef SMBLEVELWORKSHOP2_WS2COMMON_STATUS_HPP
#define SMBLEVELWORKSHOP2_WS2COMMON_STATUS_HPP

namespace WS2Common {
    enum EnumStatus {
        SUCCESS,
        FAILURE,
        UNKNOWN
    };
}

#endif

